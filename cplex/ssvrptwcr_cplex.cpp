/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



#include <ilcplex/ilocplex.h>
#include "VRP_instance.h"
#include "VRP_solution.h"
#include "tools.h"

#include <assert.h>
#include <cmath>
#include <iomanip>  // setw, setfill
#include <cstdlib>
#include <cstdio>	// sprintf
#include <limits>   // numeric_limits

#include <boost/math/special_functions/prime.hpp> // prime

using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	if (operator==(other)) return false;
	if (revealTime < other.revealTime)              // reveal time first
	return true;
	if (revealTime == other.revealTime) {
		if (l < other.l)                    // end of time window second)
			return true;
		if (l == other.l) {
			ASSERT(vertex->region != other.vertex->region, "no tie breaking !");
			return vertex->region < other.vertex->region;   // region number to break ties
		}
	}
	return false;
}

string itos(int i) {stringstream s; s << i; return s.str(); }

Solution_SS_VRPTW_CR* best_sol;
Solution_SS_VRPTW_CR* current_sol;
VRP_instance *I;

int nb_feasible_solutios = 0;
int nb_optimality_cuts = 0;

ILOSTLBEGIN


// #define	WAIT_TIME_MIN		10
// #define WAIT_TIME_MULTIPLE	10
// #define	WAIT_TIME_MAX		20

// int MIPSOL_SSVRPTW::get_IloNextBranchVar(IloNumVarArray vars_, IloArray< IloArray< IloArray<IloIntVar> > > ilo_vars_x, IloArray< IloArray< IloArray<IloIntVar> > > ilo_vars_tau) {
// 	int *curr_ro = new int[NVeh+1];
// 	for (int k=1; k<NVeh+1; k++)
// 		curr_ro[k] = 0;		
// 	int *curr_t = new int[NVeh+1];
// 	for (int k=1; k<NVeh+1; k++)
// 		curr_t[k] = 1;	
// 	bool *drive = new bool[NVeh+1];
// 	for (int k=1; k<NVeh+1; k++)
// 		curr_ro[k] = true;		


// 	// ostringstream *out = new ostringstream[NVeh+1]; 
// 	// for (int k=1; k<NVeh+1; k++) {
// 	// 	out[k].setf(std::ios::fixed);
// 	// 	out[k] << "D";
// 	// }



// 	// first check if there is a fractional decision associated to leaving the depot
// 	bool found = false;
// 	for (int p=1; p < NVeh+1; p++) {
// 		for (int j=1; j<NVertices+1; j++) {
// 			if (0.001 < x[0][j][p] && x[0][j][p] < 0.999) {
// 				vars_.add(ilo_vars_x[0][j][p]);
// 				found = true;

// 				// out[p] << "\t" << j << " (f) t=" << d[0][j];
// 				// for (int k=1; k<NVeh+1; k++)
// 				// 	cout << "Route " << k << ":\t" << out[k].str() << endl;

// 				delete [] curr_ro; delete [] drive; delete [] curr_t;
// 				return 1;
// 			} else if (x[0][j][p] >= 0.999) {
// 				curr_ro[p] = j;
// 				curr_t[p] += d[0][j];
// 				drive[p] = false;
// 				found = true;

// 				// out[p] << "\t" << j << " (i) t=" << d[0][j];
// 				break;
// 			}
// 		}
// 	}

// 	bool finished = false;
// 	while (!finished) {
// 		ASSERT(found, "Nothing found");
// 		found = false;

// 		// Find the route for which its next action (either drive or wait) that gives the least current time  
// 		int min_t = H+1;
// 		int min_k = 0, min_i, min_j, min_l;
// 		bool min_drive;
// 		for (int k=1; k<NVeh+1; k++) {
// 			if (curr_ro[k] == 0) continue;
// 			if (drive[k]) {		// if next planned action is drive			
// 				for (int j=0; j<NVertices+1; j++) {
// 					if (0.001 < x[curr_ro[k]][j][k] && x[curr_ro[k]][j][k] < 0.999) {
// 						int new_t_k_j = curr_t[k] + d[curr_ro[k]][j];
// 						if (new_t_k_j <= min_t) {
// 							min_t = new_t_k_j;
// 							min_k = k;
// 							min_i = curr_ro[k];
// 							min_j = j;
// 							min_drive = true;
// 							found = true;
// 						}
// 					} else if (x[curr_ro[k]][j][k] >= 0.999) {
// 						curr_t[k] += d[curr_ro[k]][j];
// 						curr_ro[k] = j;
// 						found = true;
// 						// out[k] << "\t" << j << "(i) t=" << curr_t[k];
// 						break;
// 					}
// 				}
// 			}
// 			else {		// if next planned action is wait
// 				for (int l=0; l<H+1; l++) {
// 					if (0.001 < tau[curr_ro[k]][l][k] && tau[curr_ro[k]][l][k] < 0.999) {
// 						int new_t_k_l = curr_t[k] + l ;
// 						if (new_t_k_l <= min_t) {
// 							min_t = new_t_k_l;
// 							min_k = k;
// 							min_i = curr_ro[k];
// 							min_l = l;
// 							min_drive = false;
// 							found = true;
// 						}
// 					} else if (tau[curr_ro[k]][l][k] >= 0.999) {
// 						curr_t[k] += l;
// 						found = true;
// 						// out[k] << "\tw" << curr_ro[k] << ":" << l << "(i) t=" << curr_t[k];
// 						break;
// 					}
// 				}

// 			}
// 			drive[k] = !drive[k];

// 		}	// now min_k contains the route number k that leads to the least resulting time (min_t) when performing its next planned action (either wait or drive)
		
// 		if (min_k > 0) {		// if we found a fractional minimal action
// 			if (min_drive) {
// 				vars_.add(ilo_vars_x[min_i][min_j][min_k]);
// 				// out[min_k] << "\t" << min_j << "(f) t=" << min_t;
// 			} 
// 			else {
// 				vars_.add(ilo_vars_tau[min_i][min_l][min_k]);
// 				// out[min_k] << "\tw" << min_i << ":" << min_l << "(f) t=" << min_t;
// 			}
// 			delete [] curr_ro; delete [] drive; delete [] curr_t;

// 			// for (int k=1; k<NVeh+1; k++)
// 			// 	cout << "Route " << k << ":\t" << out[k].str() << endl;

// 			return 1;
// 		}

// 		finished = true;
// 		for (int k = 1; k < NVeh+1; k++)			// are all the vehicle returned to the depot ?
// 			if (curr_ro[k] != 0)
// 				finished = false;
// 	}

// 	// for (int k=1; k<NVeh+1; k++)
// 	// 			cout << "Route " << k << ":\t" << out[k].str() << endl;

// 	delete [] curr_ro; delete [] drive; delete [] curr_t;
// 	return 0;
// }



// ILOBRANCHCALLBACK4(BranchCallback, IloArray< IloArray< IloArray<IloIntVar> > >, x_vars, IloArray< IloArray< IloArray<IloIntVar> > >, tau_vars, IloArray< IloArray<IloIntVar> >, y_vars, IloFloatVar, theta_var) {
// 	IloEnv env = getEnv();

// 	// cout << "Branch callback called." << endl;


// 	IloInt i, j,l, p;

// 	MIPSOL_SSVRPTW sol;
// 	// RETRIEVE vars_x
// 	for (int i = 0; i < NVertices+1; i++) 
// 		for (int j = 0; j < NVertices+1; j++) 
// 			for (p=1; p<NVeh+1; p++)
// 				sol.setX(i, j, p, getValue(x_vars[i][j][p]));	
// 	// RETRIEVE vars_tau
// 	for (int i = 1; i < NVertices+1; i++) 
// 		for (int l = 1; l < H+1; l++) 
// 			for (p=1; p<NVeh+1; p++)
// 				sol.setTAU(i,l,p, getValue(tau_vars[i][l][p]));
// 	// RETRIEVE vars_y
// 	for (int i = 0; i < NVertices+1; i++) 
// 		for (p=1; p<NVeh+1; p++)
// 			sol.setY(i, p, getValue(y_vars[i][p]));


// 	// cout << sol.toStringVariables() << endl << endl;


// 	IloNumVarArray vars(env);
// 	IloNumArray bounds(env);
// 	IloCplex::BranchDirectionArray dirs(env);
// 	for (i = 0; i < getNbranches(); i++) {
// 		getBranch(vars, bounds, dirs, i);	

// 		// cout << "Branch: " << vars << "   " << bounds << "   " << dirs << endl;

// 	}
// 	vars.end(); bounds.end(); dirs.end();
	

// 	IloNumVarArray vars_(env);
// 	if (sol.get_IloNextBranchVar(vars_, x_vars, tau_vars)) {
// 		// cout << "Should rather branch on " << vars_[0] << endl;
// 		ASSERT(0.001 < getValue(vars_[0]) && getValue(vars_[0]) < 0.999, vars_[0] << "= " << getValue(vars_[0]) << " not fractional.");
// 		makeBranch(vars_[0], 1, IloCplex::BranchUp, getObjValue());
// 		makeBranch(vars_[0], 0, IloCplex::BranchDown, getObjValue());
// 	}	
// 	vars_.end();



// 	return;
// }


ILOLAZYCONSTRAINTCALLBACK4(SubtourEliminationCallback, IloArray< IloArray< IloArray<IloIntVar> > >, x_vars, IloArray< IloArray< IloArray<IloIntVar> > >, tau_vars, IloArray< IloArray<IloIntVar> >, y_vars, IloFloatVar, theta_var) {
	IloEnv env = getEnv();


	// cout << "Lazy constraint callback called." << endl;


	int NVertices = I->getNumberVertices(VRP_vertex::WAITING);
	int NVeh = I->getNumberVehicles();
	int H = I->getHorizon();
	// cout << endl << "MIPSOL" << endl;
	// RETRIEVE x_vars
	// cout << "Retrieve x_vars" << endl;
	for (int i = 0; i < NVertices+1; i++) 
		for (int j = 0; j < NVertices+1; j++)
			for (int p = 1; p < NVeh+1; p++) {
				// cout << "(" << i << "," << j << "," << p << "): " << getValue(x_vars[i][j][p]) << "  ";
				current_sol->mipsol_setX(i, j, p, getValue(x_vars[i][j][p]));
			}
	// cout << endl;

	current_sol->update_routes_from_mipsol();

	// cout << "Current sol: \t";
	// for (auto& arc : current_sol->mipsol_getArcs())
	// 	cout << "(" << arc.i << "," << arc.j << "," << arc.p << ") ";
	// cout << endl;

	// RETRIEVE tau_vars
	// cout << "Retrieve tau_vars" << endl;
	for (int i = 1; i < NVertices+1; i++) 
		for (int l = 1; l < H+1; l++)
			for (int p = 1; p < NVeh+1; p++) {
				// cout << "tau_vars(" << i << "," << l << "," << p << ")" << tau_vars[i][l][p] << "  ";
				current_sol->mipsol_setTAU(i,l,p, getValue(tau_vars[i][l][p]));
			}
				

	

	

	// SECs
	const vector<vector<Solution_SS_VRPTW_CR::MIPSol_Arc>> & subtours = current_sol->mipsol_getSubtours();
	for (auto& subtour : subtours) {
		// Add subtour elimination constraint for each route p
		for (int p = 1; p < NVeh+1; p++) {
			IloExpr expr(env);
			for (auto& arc : subtour)
				expr += x_vars[arc.i][arc.j][p];
			add(expr <= (int) subtour.size()-1).end();
			expr.end();
		}
	}


	IloNum theta = getValue(theta_var);



	if (subtours.size() == 0) {
		nb_feasible_solutios ++;

		current_sol->update_waitingTimes_from_mipsol();

		// cout << "WAIT. TIMES:\t";
		// for (auto& wt : current_sol->mipsol_getWaitTimes())
		// 	cout << "(" << wt.i << "," << wt.l << "," << wt.p << ") ";
		// cout << endl;
		// cout << current_sol->toString() << endl;

		/* Specific OPTIMALITY CUT ------------------------------------- */
		double E = current_sol->getCost();

		ASSERT(current_sol->getNumberViolations() == 0, current_sol->toString(true));

		if (theta < E) {
			IloExpr expr_x(env); 
			IloExpr expr_tau(env);

			const vector< Solution_SS_VRPTW_CR::MIPSol_Arc > & arcs = current_sol->mipsol_getArcs();
			for (auto& arc : arcs)
				expr_x += x_vars[arc.i][arc.j][arc.p];
			
			const vector< Solution_SS_VRPTW_CR::MIPSol_WaitTime > & taus = current_sol->mipsol_getWaitTimes();
			for (auto& tau : taus)
				expr_tau += tau_vars[tau.i][tau.l][tau.p];

			add(theta_var >= E * ((expr_x + expr_tau) - (arcs.size() + taus.size()) + 1) );
			// add( expr_x + expr_tau <= (int) (arcs.size() + taus.size() - 1 ));
			nb_optimality_cuts ++;

		 	expr_x.end(); expr_tau.end();

			if (current_sol->getCost() < best_sol->getCost()) {
				// add( theta_var <= E );
				*best_sol = *current_sol;
				// cout << "New incumbent: " << current_sol->getCost() << endl;
			}
		}
		 
		// // Generating GENERAL OPTIMALITY CUT
		// if (sol.getCost() > best_sol->getCost()) {
		// 	MIPSOL_SSVRPTW partial_sol;
		// 	double E_partial = s.computeExpectedCost_withOptimalityCuts(best_sol->getCost(), sol, &partial_sol);

		// 	IloExpr expr_x(env); IloExpr expr_tau(env);
		// 	int sum_x =0, sum_tau=0;

		// 	partial_sol.get_IloLinExpr_val_to_1(expr_x, expr_tau, x_vars, tau_vars, &sum_x, &sum_tau, true);
		// 	add(theta_var >= E_partial * ((expr_x + expr_tau) - (sum_x+sum_tau) + 1) );

		// 	expr_x.end(); expr_tau.end();
		// }


		// /* Specific + general OPTIMALITY CUT (two-in-one version) ---------------------------- */
		// MIPSOL_SSVRPTW partial_sol;

		// // double theta_max = max(best_sol->getCost(), theta);
		// double theta_max = best_sol->getCost();
		// // int *last_wait_ro = new int [NVeh +1];
		// // int *last_wait_w = new int [NVeh +1];
		// // double E_partial = s.computeExpectedCost_withOptimalityCuts(theta_max, sol, &partial_sol, last_wait_ro, last_wait_w);
		// double E_partial = s.computeExpectedCost_withOptimalityCuts(theta_max, sol, &partial_sol);
		// bool completed = (E_partial < theta_max);

		// if (E_partial < best_sol->getCost()) {
		//   *best_sol = sol;
		//   best_sol->setCost(E_partial);
		//   // int sum_x, sum_tau;
		//   // IloExpr expr_x(env); IloExpr expr_tau(env);
		//   // partial_sol.get_IloLinExpr_val_to_1(expr_x, expr_tau, x_vars, tau_vars, &sum_x, &sum_tau, true);
		//   // add( expr_x + expr_tau <= sum_x + sum_tau - 1 );
		//   // add( theta_var <= E_partial );
		//   // expr_x.end(); expr_tau.end();
		//   // cout << "New incumbent: " << sol.getCost() << endl;
		// }
		// if (theta < E_partial) {
		// 	int sum_x, sum_tau;

		// 	IloExpr expr_x(env); IloExpr expr_tau(env);
		// 	partial_sol.get_IloLinExpr_val_to_1(expr_x, expr_tau, x_vars, tau_vars, &sum_x, &sum_tau, true);
		// 	add(theta_var >= E_partial * ((expr_x + expr_tau) - (sum_x+sum_tau) + 1) );

		// 	// // dEBUG -----------
		// 	// double exp_ = s.computeExpectedCost();
		// 	// IloExpr _expr_x(env); IloExpr _expr_tau(env); int _sum_x_, _sum_tau_;
		// 	// sol.get_IloLinExpr_val_to_1(_expr_x, _expr_tau, x_vars, tau_vars, &_sum_x_, &_sum_tau_, true);
		// 	// if (sum_x + sum_tau != _sum_x_ + _sum_tau_) {
		// 	// 	cout << LINE << sol.toStringVariables() << endl << "ExpCost: " << exp_ << "    (best: " << best_sol->getCost() << ")" << endl;
		// 	// 	cout << _expr_x << endl << _expr_tau << endl << endl;
		// 	// 	cout << partial_sol.toStringVariables() << endl << "E_partial: " << E_partial << endl;
		// 	// 	cout << expr_x << endl << expr_tau << endl << "sum_x=" << _sum_x_ << "  sum_tau=" << _sum_tau_ << endl << endl;
		// 	// }
		// 	// if (completed)
		// 	// 	ASSERT(E_partial-0.001 <= exp_ && exp_ <= E_partial+0.001, "E_partial=" << E_partial << "\t exp_=" << exp_);
		// 	// // ------- end dEBUG


			
			
		// 	// delete [] last_wait_ro; delete [] last_wait_w;
		// 	expr_x.end(); expr_tau.end();
		// }
		// cout << "--------------------------------------------------------------------------------------------" << endl;

	}

	return;
}

// ILOUSERCUTCALLBACK4(UserCutCallback, IloArray< IloArray< IloArray<IloIntVar> > >, x_vars, IloArray< IloArray< IloArray<IloIntVar> > >, tau_vars, IloArray< IloArray<IloIntVar> >, y_vars, IloFloatVar, theta_var) {
// 	IloEnv env = getEnv();

// 	cout << "User cut callback called." << endl;


// 	MIPSOL_SSVRPTW sol;
// 	double *sol_veh = new double[NVeh+1];
// 	// RETRIEVE vars_x
// 	for (int i = 0; i < NVertices+1; i++) {
// 		for (int j = 0; j < NVertices+1; j++) {
// 			for (int p=1; p<NVeh+1; p++)
// 				sol_veh[p] = getValue(x_vars[i][j][p]);
// 			sol.setX_i_j(i, j, sol_veh);
// 		}
// 	}		
// 	// RETRIEVE vars_tau
// 	for (int i = 1; i < NVertices+1; i++) {
// 		for (int l = 1; l < H+1; l++) {
// 			for (int p=1; p<NVeh+1; p++)
// 				sol_veh[p] = getValue(tau_vars[i][l][p]);
// 			sol.setTAU_i_l(i, l, sol_veh);
// 		}
// 	}
// 	delete [] sol_veh;	

// 	cout << sol.toStringVariables() << endl << endl;

// 	return;
// }


int main(int argc, char **argv) {
	IloEnv env;

	if (argc < 9) {
		cout << "Usage: ssvrptwcr_cplex travel_times_file instance_file recourse_strategy number_vehicles vehicle_capacity waiting_time_multiple time_limit scale" << endl;
		return 1;
	}
	const char * travel_times_file = argv[1];
	const char * instance_file = argv[2];
	const string recourse_strategy_str(argv[3]);
	const int number_vehicles = atoi(argv[4]);
	const int vehicle_capacity = atoi(argv[5]);
	const int waiting_time_multiple = atoi(argv[6]);
	const int time_limit = atoi(argv[7]);
	const int scale = atoi(argv[8]);

	RecourseStrategy strategy;
	if (recourse_strategy_str == "R_INFTY") 			strategy = R_INFTY;
	else if (recourse_strategy_str == "R_CAPA") 		strategy = R_CAPA;
	else if (recourse_strategy_str == "R_CAPA_PLUS") 	strategy = R_CAPA_PLUS;
	else ASSERT(false, "wrong recourse strategy: " << recourse_strategy_str);

	cout << endl;
	srand(time(NULL));
	// I = & VRP_instance_file::readInstanceFile_SSVRPTW_CR(instance_file, 1.0);
	// I = & VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1(travel_times_file, instance_file);
	I = & VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, scale);
	I->setNumberVehicles(number_vehicles);
	I->setVehicleCapacity(vehicle_capacity);

	cout << "Instance file: " << instance_file << endl;
	cout << I->toString() << endl;
	cout << "RecourseStrategy : " << recourse_strategy_str << endl;
	cout << "Wait. time multiple:\t" << setw(3) << waiting_time_multiple << endl;

	current_sol = new Solution_SS_VRPTW_CR(*I, strategy);
	current_sol->generateInitialSolution(true);

	best_sol = new Solution_SS_VRPTW_CR(*I, strategy);
	best_sol->generateInitialSolution(true);
	cout << endl;

	try {
		int NVertices = I->getNumberVertices(VRP_vertex::WAITING);
		int NVeh = I->getNumberVehicles();
		int H = I->getHorizon();

		// create model

		IloModel ssvrptwcr(env);
		
		char varname[128];

		IloArray< IloArray< IloArray<IloIntVar> > > vars_x(env, NVertices+1);
		for (int i = 0; i < NVertices+1; i++) {
			vars_x[i] = IloArray< IloArray<IloIntVar> >(env, NVertices+1);
			for (int j = 0; j < NVertices+1; j++) {
				vars_x[i][j] = IloArray<IloIntVar>(env, NVeh+1);
				for (int p = 1; p < NVeh+1; p++) {
					sprintf(varname, "x_%d_%d_%d", i, j, p);
					vars_x[i][j][p] = IloIntVar(env, 0, 1, varname);
				}
			}
		}

		IloArray< IloArray<IloIntVar> > vars_y(env, NVertices+1);
		for (int i = 0; i < NVertices+1; i++) {
			vars_y[i] = IloArray<IloIntVar>(env, NVeh+1);
			for (int p = 1; p < NVeh+1; p++) {
				sprintf(varname, "y_%d_%d", i, p);
				vars_y[i][p] = IloIntVar(env, 0, 1, varname);
			}
		}


		IloArray< IloArray< IloArray<IloIntVar> > > vars_tau(env, NVertices+1);
		for (int i = 1; i < NVertices+1; i++) {
			vars_tau[i] = IloArray< IloArray<IloIntVar> >(env, H+1);
			for (int l = 1; l < H+1; l++){
				vars_tau[i][l] = IloArray<IloIntVar>(env, NVeh+1);
				for (int p = 1; p < NVeh+1; p++) {
					sprintf(varname, "t_%d_%d_%d", i, l, p);
					vars_tau[i][l][p] = IloIntVar(env, 0, 1, varname);
				}
			}
		}

		IloFloatVar var_theta(env, 0, 100, "theta");
		ssvrptwcr.add(var_theta >= 0);

		ssvrptwcr.add(IloMinimize(env, var_theta));
		// IloExpr obj(env);
		// for (i = 1; i < NVertices+1; i++)
		// 	for (j = 1; j < NVertices+1; j++)
		// 		for (p = 1; p < NVeh+1; p++)
		// 			obj += vars_x[i][j][p] * d[i][j];
		// ssvrptwcr.add(IloMinimize(env, obj));



		// ****************** Constraints *******************


		// Flow conservation constraints for customer vertices
		for (int i = 0; i < NVertices+1; i++) {
			for (int p = 1; p < NVeh +1; p++) {
				IloExpr expr(env); IloExpr expr_(env);
				for (int j = 0; j < NVertices+1; j++) {
					if (j == i) continue;
					expr += vars_x[i][j][p];
					expr_ += vars_x[j][i][p];
				}
				ssvrptwcr.add(expr == vars_y[i][p]);
				ssvrptwcr.add(expr_ == vars_y[i][p]);
				expr.end(); expr_.end();
			}
		}  

		// Depot vertex visited NVeh times
		IloExpr expr(env);
		for (int p = 1; p < NVeh +1; p++)
			expr += vars_y[0][p];
		ssvrptwcr.add(expr == NVeh);
		expr.end();
		
		// Each vertex visited by at most one route
		for (int i = 1; i < NVertices+1; i++) {
			IloExpr expr(env);
			for (int p = 1; p < NVeh +1; p++)
        		expr += vars_y[i][p];
        	ssvrptwcr.add(expr <= 1);
        	expr.end();
		}

		// Forbid edge from node back to itself
		for (int i = 0; i < NVertices+1; i++)
			for (int p = 1; p < NVeh +1; p++)
				ssvrptwcr.add(vars_x[i][i][p] == 0);



		// WAITING TIMES ------------

		// Exactly one waiting time selected by visited waiting vertex
		for (int i = 1; i < NVertices+1; i++) {
			for (int p = 1; p < NVeh +1; p++) {
				IloExpr expr(env);
				for (int l = 1; l < H+1; l++) 
					expr += vars_tau[i][l][p];
				ssvrptwcr.add(expr == vars_y[i][p]);
				expr.end();
			}
		}

		// Max duration of each route p
		for (int p = 1; p < NVeh +1; p++) {
			IloExpr expr(env);

			for (int i = 0; i < NVertices+1; i++) 
				for (int j = 0; j < NVertices+1; j++) {
					if (i == j) continue;
					expr += I->travelTime(i, j) * vars_x[i][j][p];
				}

			for (int i = 1; i < NVertices+1; i++)
				for (int l = 1; l < H+1; l++) 
					expr += vars_tau[i][l][p] * l;

			ssvrptwcr.add(expr <= H - 1);
			expr.end();
		}


		// SYMMETRY BREAKING --------------
		// lex ordering
		for (int p = 1; p < NVeh+1 -1; p++) {
			IloExpr expr(env); IloExpr expr_(env);
			for (int i = 1; i < NVertices+1; i++) {
				expr += pow(2.0, (double) i) * vars_y[i][p];
				expr_ += pow(2.0, (double) i) * vars_y[i][p+1];
				// expr += log(boost::math::prime(i)) * vars_y[i][p];
				// expr_ += log(boost::math::prime(i)) * vars_y[i][p+1];
			}
			ssvrptwcr.add(expr <= expr_ );
			expr.end(); expr_.end();
		}

		// RESTRICTING WAITING TIMES  ----------------
		// Restrict waiting times 
		for (int i = 1; i < NVertices+1; i++)
			for (int l = 1; l < H+1; l++)
				if ( (l % waiting_time_multiple) != 0 )
				// if ( l != I->getWaitingTimeMultiple() )
				// if ( l != I->getWaitingTimeMultiple() && l != 2*I->getWaitingTimeMultiple() )
					for (int p = 1; p<NVeh+1; p++)
						ssvrptwcr.add(vars_tau[i][l][p] == 0);



		IloCplex cplex(ssvrptwcr);

		// IloNum tol = cplex.getParam(IloCplex::EpInt);
		// IloCplex::Callback sec = cplex.use(SubtourEliminationCallback(env, vars_x, vars_tau, vars_y, var_theta));
		cplex.use(SubtourEliminationCallback(env, vars_x, vars_tau, vars_y, var_theta));
		// IloCplex::Callback branch = cplex.use(BranchCallback(env, vars_x, vars_tau, vars_y, var_theta));
		// IloCplex::Callback usercut = cplex.use(UserCutCallback(env, vars_x, vars_tau, vars_y, var_theta));

		// Disabling PRESOLVE (why ? variable mapping ? seems better with presolve)
		// cplex.setParam(IloCplex::PreInd, IloFalse);

		// set time limit
		cplex.setParam(IloCplex::Param::TimeLimit, time_limit);

		if ( cplex.solve() ) {
			// env.out() << "Optimal value: " << cplex.getObjValue() << endl;

			// IloNum theta = cplex.getValue(var_theta);
			// cout << "theta = " << theta << endl;

			
			// RETRIEVE vars_x
			for (int i = 0; i < NVertices+1; i++) 
				for (int j = 0; j < NVertices+1; j++) 
					for (int p = 1; p < NVeh+1; p++)
						current_sol->mipsol_setX(i, j, p, cplex.getValue(vars_x[i][j][p]));	
			// RETRIEVE vars_tau
			for (int i = 1; i < NVertices+1; i++) 
				for (int l = 1; l < H+1; l++) 
					for (int p = 1; p < NVeh+1; p++)
						current_sol->mipsol_setTAU(i,l,p, cplex.getValue(vars_tau[i][l][p]));

			current_sol->update_from_mipsol();
			cout << "Selected solution: " << current_sol->getCost() << endl << current_sol->toString() << endl;
			// cout << current_sol->toString(true) << endl;
		} else {
			cout << "NO FEASIBLE SOLUTION !" << endl;
		}
		cout << "Instance file: " << instance_file << endl;
		cout << endl << "Best scaled solution recorded: " << best_sol->getCost() << endl << best_sol->toString() << endl; 
		// cout << best_sol->toString(true) << endl;


		VRP_instance& I480 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, 1.0);
		I480.setNumberVehicles(number_vehicles);
		I480.setVehicleCapacity(vehicle_capacity);
		cout << "Solution under 480 horizon: " << endl;
		Solution_SS_VRPTW_CR s480(I480, strategy);
		s480.generateInitialSolution(true);
		s480.copyFromSol_scaleWaitingTimes(*best_sol);
		cout << s480.toString(false) << s480.getCost() <<  endl;

		cout << "# 1st stage feasible solutions: " << nb_feasible_solutios << endl;
		cout << "# optimality cuts added:        " << nb_optimality_cuts << endl;
	}
	catch (const IloException& e) {
		cerr << "Exception caught: " << e << endl;
	}
	catch (...) {
		cerr << "Unknown exception caught!" << endl;
	}

	env.end();
	return 0;
}


/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/*

This program computes the average number of rejected requests under Wait-and-Serve policy, described in paper:
"The Static and Stochastic VRPTW with both random Customers and Reveal Times: algorithms and recourse strategies", Saint-Guillain et al, 2017


Handles on instance files of the type described in the same paper. 
*/



#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>

using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	if (operator==(other)) return false;

	if (revealTime < other.revealTime)							// reveal time first
		return true;
	if (revealTime == other.revealTime) {
		if (l < other.l)										// end of time window second)
			return true;
		if (l == other.l) {
			ASSERT(vertex->region != other.vertex->region, "no tie breaking !");
			return vertex->region < other.vertex->region;		// region number to break ties
		}
	}

	return false;
}



int main(int argc, char **argv) {
	if (argc < 6) {
		cout << "Usage: ssvrptwcr_recourse_basic travel_times_file instance_file recourse_strategy number_vehicles vehicle_capacity" << endl;
		return 1;
	}
	const char * travel_times_file = argv[1];
	const char * instance_file = argv[2];
	const string recourse_strategy_str(argv[3]);
	const int number_vehicles = atoi(argv[4]);
	const int vehicle_capacity = atoi(argv[5]);


	RecourseStrategy strategy;
	if (recourse_strategy_str == "R_BASIC_INFTY") 			strategy = R_BASIC_INFTY;
	else if (recourse_strategy_str == "R_BASIC_CAPA") 		strategy = R_BASIC_CAPA;
	else _ASSERT_(false, "wrong recourse strategy: " << recourse_strategy_str);

	srand(time(NULL));

	// VRP_instance & I = VRP_instance_file::readInstanceFile_SSVRPTW_CR(instance_file, 1.0);
	// VRP_instance & I = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1(travel_times_file, instance_file);		// Won't work, because ScenarioPool assumes that the depot has id 0
	VRP_instance & I = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 1.0);
	cout << "Instance file: " << instance_file;
	
	cout << I.toString();
	// cout << I.toStringCoords(true) << endl;
	// cout << I.toString_TravelTimes() << endl;
	// cout << I.toStringProbasTS() << endl << endl;

	cout << I.toStringInstanceMetrics() << endl;
	
	Solution_SS_VRPTW_CR s(I, strategy);
	s.addRoutes(number_vehicles);

	VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenarioPool(I, 1000000, SS_VRPTW_CR);
	cout << "Average cost under " << recourse_strategy_str << " recourse strategy with " << number_vehicles << " vehicle: \t" << outputMisc::redExpr(true) << scenarioPool.computeExperimentalExpectedCost(s, strategy, false, false) << outputMisc::resetColor() << endl;  // (const Solution& solution, RecourseStrategy strategy, bool debug=false, bool verbose = false, int scenarioHorizon = numeric_limits<int>::max()) 

	// VRP_instance & I_plus1veh = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, 1.0);
	// I_plus1veh.setNumberVehicles(number_vehicles + 1);
	// I_plus1veh.setVehicleCapacity(vehicle_capacity);

	// Solution_SS_VRPTW_CR s_plus1veh(I_plus1veh, strategy);

	// VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> scenarioPool_bis(I_plus1veh, 1000000);
	// cout << "Average cost under " << recourse_strategy_str << " recourse strategy with " << number_vehicles+1 << " vehicles: \t" << scenarioPool_bis.computeExperimentalExpectedCost(s_plus1veh, strategy, false, false) << endl;
}




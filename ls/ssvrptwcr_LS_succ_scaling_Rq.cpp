/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/*

The Local Search algorithm described in paper:
"The Static and Stochastic VRPTW with both random Customers and Reveal Times: algorithms and recourse strategies", Saint-Guillain et al, 2017

Handles on instance files of the type described in the same paper. 
*/



#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>
#include <ctime>	// clock()

using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	if (operator==(other)) return false;

	if (revealTime < other.revealTime)							// reveal time first
		return true;
	if (revealTime == other.revealTime) {
		if (l < other.l)										// end of time window second)
			return true;
		if (l == other.l) {
			ASSERT(vertex->region != other.vertex->region, "no tie breaking !");
			return vertex->region < other.vertex->region;		// region number to break ties
		}
	}

	return false;
}


int main(int argc, char **argv) {
	if (argc < 9) {
		cout << "Usage: ssvrptwcr_LS_succ_scaling_Rq mode[s|w|sw] travel_times_file instance_file number_vehicles vehicle_capacity waiting_time_multiple time_limit nb_runs" << endl;
		return 1;
	}
	string mode(argv[1]);
	const char * travel_times_file = argv[2];
	const char * instance_file = argv[3];
	const int number_vehicles = atoi(argv[4]);
	const int vehicle_capacity = atoi(argv[5]);
	const int waiting_time_multiple = atoi(argv[6]);
	const int time_limit = atoi(argv[7]);
	const int nb_runs = atoi(argv[8]);


	srand(time(NULL));

	// CREATE SCALED INSTANCES
	VRP_instance & I_scale1 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 1);
	VRP_instance & I_scale2 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 2);
	VRP_instance & I_scale5 = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 5);

	cout << endl;
	cout << "Instance file: " << instance_file << endl;
	cout << I_scale1.toString() << endl;
	cout << "Wait. time multiple:\t" << setw(3) << waiting_time_multiple << endl << endl;

	vector<int> wait_time_multiple_factors;
	bool succ_scale = false;
	int computation_time;
	if (mode == "s") {
		cout << "SUCESSIVE SCALING (Rq only, s5 -> s2 -> s1)" << endl;
		wait_time_multiple_factors = {1};
		succ_scale = true;
		computation_time = (int) floor(time_limit/3);
	}
	else if (mode == "w") {
		cout << "SUCESSIVE WTime Multiples (Rq only, scale 1, wmult: x6 -> x3 -> x1)" << endl;
		wait_time_multiple_factors = {6, 3, 1};
		computation_time = (int) floor(time_limit/3);
	}
	else if (mode == "ws") {
		cout << "SUCESSIVE WTime Multiples / SCALING (Rq only, wmult: x6 with {s5, s2, s1} -> x3 with {s5, s2, s1} -> x1 with {s5, s2, s1})" << endl;
		wait_time_multiple_factors = {6, 3, 1};
		succ_scale = true;
		computation_time = (int) floor(time_limit/(3*3));
	} else _ASSERT_(false, "wrong option mode");


	double avg_cost = 0.0;
	for (int run_ = 1; run_ <= nb_runs; run_++) {

		int initialWaitingTime = 60;	// one hour initial waiting time, always

		// CREATE SOLUTIONS
		Solution_SS_VRPTW_CR sol__Rq_s1(I_scale1, R_CAPA, initialWaitingTime, 6 * waiting_time_multiple);
		Solution_SS_VRPTW_CR sol__Rq_s2(I_scale2, R_CAPA, initialWaitingTime/2, 6 * waiting_time_multiple/2);
		Solution_SS_VRPTW_CR sol__Rq_s5(I_scale5, R_CAPA, initialWaitingTime/5, 6 * waiting_time_multiple/5);
		sol__Rq_s1.addRoutes(number_vehicles);
		sol__Rq_s2.addRoutes(number_vehicles);
		sol__Rq_s5.addRoutes(number_vehicles);
		sol__Rq_s1.generateInitialSolution(true);
		sol__Rq_s2.generateInitialSolution(true);
		sol__Rq_s5.generateInitialSolution(true);

		enum SeqElem {
			RQ_S5,		
			RQ_S2,		
			RQ_S1,		
		};
		const char* desc_seq[] = {"Rq_s5", "Rq_s2", "Rq_s1"};
		Solution_SS_VRPTW_CR * sol_seq [] = {&sol__Rq_s5, &sol__Rq_s2, &sol__Rq_s1};
		
	
		int prev_algo = -1;
		Solution_SS_VRPTW_CR *current_sol;
		for (int i : wait_time_multiple_factors) {


			// CREATE NEIGHBORHOOD MANAGERS
			NeighborhoodManager<Solution_SS_VRPTW_CR> nm__Rq_scale1(sol__Rq_s1, SS_VRPTW_CR, I_scale1.getHorizon(), i * waiting_time_multiple, ceil(I_scale1.getHorizon() / 4));
			NeighborhoodManager<Solution_SS_VRPTW_CR> nm__Rq_scale2(sol__Rq_s2, SS_VRPTW_CR, I_scale2.getHorizon(), i * waiting_time_multiple/2, ceil(I_scale2.getHorizon() / 4));
			NeighborhoodManager<Solution_SS_VRPTW_CR> nm__Rq_scale5(sol__Rq_s5, SS_VRPTW_CR, I_scale5.getHorizon(), i * waiting_time_multiple/5, ceil(I_scale5.getHorizon() / 4));

			NeighborhoodManager<Solution_SS_VRPTW_CR> * nm_seq [] = {&nm__Rq_scale5, &nm__Rq_scale2, &nm__Rq_scale1};

			int curr_algo;
			if (succ_scale) curr_algo = RQ_S5;
			else curr_algo = RQ_S1;
			for (; curr_algo <= RQ_S1; curr_algo++) {
				if (prev_algo != -1 && succ_scale) {
					cout << "copying solution from " << desc_seq[prev_algo] << " into " << desc_seq[curr_algo] << endl;
					sol_seq[curr_algo]->copyFromSol_scaleWaitingTimes(*sol_seq[prev_algo], true);
				}

				current_sol = sol_seq[curr_algo];
				NeighborhoodManager<Solution_SS_VRPTW_CR> *current_nm = nm_seq[curr_algo];
				const char * current_desc = desc_seq[curr_algo];

				
				int wmult = (waiting_time_multiple / current_sol->getScale()) * i ;
				current_sol->setWaitingTimeMultiple(wmult);
				cout << endl << "Optimizing " << current_desc << " [wmult=" << wmult << "] with initial solution (cost = " << current_sol->getCost() << ") for " << computation_time << "s:" << endl << current_sol->toString();
				LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
				// LS_Program_Basic<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
				lsprog.setTimeOut(computation_time);
				lsprog.run(*current_sol, *current_nm, numeric_limits<int>::max(), 2.0, 0.95, true);	// LS_Program_SimulatedAnnealing args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
				// lsprog.run(*current_sol, *current_nm, numeric_limits<int>::max(), 0);	// LS_Program_Basic args: (Solution &solution, NM &nm, long long max_iter, int diversification = 10, int max_distance = 2500, bool verbose = false)
				cout << "Done ! Cost: " << current_sol->getCost() << "   Sol: " << endl << current_sol->toString() << endl << endl;	

				prev_algo = curr_algo;
			}
		}

		Solution_SS_VRPTW_CR s_RCAPAPLUS(I_scale1, R_CAPA_PLUS);
		s_RCAPAPLUS.addRoutes(number_vehicles);
		s_RCAPAPLUS.generateInitialSolution(true);
		s_RCAPAPLUS.copyFromSol_scaleWaitingTimes(sol__Rq_s1);
		cout << "Run #" << run_ << " : Cost under scale 1 (R_CAPA_PLUS) ==> " << s_RCAPAPLUS.getCost() <<  endl;

		avg_cost += s_RCAPAPLUS.getCost() / nb_runs;
	}
	cout << endl << "Avg cost / " << nb_runs << " runs: " << avg_cost << endl;


}




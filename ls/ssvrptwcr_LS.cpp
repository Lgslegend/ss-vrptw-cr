/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




#include "VRP_instance.h"
#include "VRP_solution.h"
#include "VRP_scenario.h"
#include "NM.h"
#include "LS_Program.h"
#include "tools.h"
#include <queue>
#include <cmath>

using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	if (operator==(other)) return false;

	if (revealTime < other.revealTime)							// reveal time first
		return true;
	if (revealTime == other.revealTime) {
		if (l < other.l)										// end of time window second)
			return true;
		if (l == other.l) {
			ASSERT(vertex->region != other.vertex->region, "no tie breaking !");
			return vertex->region < other.vertex->region;		// region number to break ties
		}
	}

	return false;
}


RecourseStrategy strategy;

VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> *scenarioPool_big;

VRP_instance* I480;
int scale = 1;

Solution_SS_VRPTW_CR *s480;
Solution_SS_VRPTW_CR *bestSol;
double cost480_bestSol;

CallbackReturnType printBestSolutionCost(Solution_SS_VRPTW_CR& bestSolution, Solution_SS_VRPTW_CR& incumbentSolution, double elapsed_time) {
	(void) incumbentSolution;
	(void) bestSolution;
	(void) elapsed_time;
	// Solution_SS_VRPTW_CR s480(*I480, strategy);
	// s480.initializeFromSol_scaleWaitingTimes(bestSolution, scale);
	// // cout << elapsed_time << "\t" << bestSolution.getCost() << "\t" << s480.getCost() << endl;
	// cout << s480.getCost() << endl;
	cout << cost480_bestSol << endl;
	return NONE;
}



CallbackReturnType newBestSolution(Solution_SS_VRPTW_CR& bestSolution) {
	cout << endl << bestSolution.toString();
	// if (scale > 1) {
	// 	// Solution_SS_VRPTW_CR s480(*I480, strategy);
	// 	s480->copyFromSol_scaleWaitingTimes(bestSolution);
	// 	if (s480->getCost() < cost480_bestSol) {
	// 		cost480_bestSol = s480->getCost();
	// 		*bestSol = bestSolution;
	// 		// cout << "  New best solution found: " << cost480_bestSol << " ("<< bestSolution.getCost() <<")" << endl;
	// 	} else {
	// 		bestSolution = *bestSol;
	// 	}
	// } else {
	// 	cost480_bestSol = bestSolution.getCost();
	// }
	return NONE;
}



int main(int argc, char **argv) {
	if (argc < 11) {
		cout << "Usage: ssvrptwcr_LS nb_runs verbose[1|0] travel_times_file instance_file recourse_strategy number_vehicles vehicle_capacity waiting_time_multiple time_limit scale" << endl;
		return 1;
	}
	const int nb_runs = atoi(argv[1]);
	const bool verbose = atoi(argv[2]);
	const char * travel_times_file = argv[3];
	const char * instance_file = argv[4];
	const string recourse_strategy_str(argv[5]);
	const int number_vehicles = atoi(argv[6]);
	const int vehicle_capacity = atoi(argv[7]);
	const int waiting_time_multiple = atoi(argv[8]);
	const int time_limit = atoi(argv[9]);
	scale = atoi(argv[10]);

	// RecourseStrategy strategy;
	if (recourse_strategy_str == "R_INFTY") 			strategy = R_INFTY;
	else if (recourse_strategy_str == "R_CAPA") 		strategy = R_CAPA;
	else if (recourse_strategy_str == "R_CAPA_PLUS") 	strategy = R_CAPA_PLUS;
	else ASSERT(false, "wrong recourse strategy: " << recourse_strategy_str);


	srand(time(NULL));
	// int nb_runs = 1;

	I480 = & VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, 1.0);
	// I480->setNumberVehicles(number_vehicles);
	// I480->setVehicleCapacity(vehicle_capacity);
	s480 = new Solution_SS_VRPTW_CR(*I480, strategy);
	s480->addRoutes(number_vehicles);
	s480->generateInitialSolution(true);

	// cout << "Instance file: " << instance_file << " \t Timeout: " << timeout << "s" << " \t scale: " << scale << endl;
	VRP_instance & I = VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(travel_times_file, instance_file, vehicle_capacity, scale);
	// I.setNumberVehicles(number_vehicles);
	// I.setVehicleCapacity(vehicle_capacity);
	bestSol = new Solution_SS_VRPTW_CR(I, strategy);
	bestSol->addRoutes(number_vehicles);
	
	cout << endl;
	cout << "Instance file: " << instance_file << endl;
	cout << I.toString() << endl;
	cout << "RecourseStrategy : " << recourse_strategy_str << endl;
	cout << "Wait. time multiple:\t" << setw(3) << waiting_time_multiple << endl;
	// cout << I.toStringCoords(true) << endl;
	// cout << I.toString_TravelTimes() << endl;
	// cout << I.toStringProbasTS() << endl << endl;
	double avg_cost = 0.0;
	double avg_cost_RCAPAPLUS = 0.0;
	for (int i = 1; i <= nb_runs; i++) {
		// SOLUTION 
		// int x = rand() % (int) floor((I.getHorizon()/2) / waiting_time_multiple) + 1;
		// int initialWaitingTime = waiting_time_multiple * x;
		// int initialWaitingTime = waiting_time_multiple;
		int initialWaitingTime = max(waiting_time_multiple, (int)floor(60 / scale));
		// if (waiting_time_multiple == 1)
		// 	initialWaitingTime = floor(I.getHorizon() / 10);

		Solution_SS_VRPTW_CR s(I, strategy, initialWaitingTime);
		s.setWaitingTimeMultiple(waiting_time_multiple);
		s.addRoutes(number_vehicles);

		// NEIGHBORHOOD MANAGER
		int minIncrement = waiting_time_multiple;
		int maxIncrement;
		if (waiting_time_multiple == 1) 
			maxIncrement = ceil(I.getHorizon() / 5);
		else 
			maxIncrement = I.getHorizon()/5;
		NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, SS_VRPTW_CR, I.getHorizon(), minIncrement, maxIncrement);
		// NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, "VRP_basic");


		// MAIN
		s.generateInitialSolution(true);
		// cout << s.toString() << endl;
		// Solution_SS_VRPTW_CR bestSol = s;	
		*bestSol = s;
		s480->copyFromSol_scaleWaitingTimes(s);
		cost480_bestSol = s480->getCost();

		// cout << "Initial solution:" << endl << s.toString(false) << endl;
		// cout << "Cost initial solution: " << s.getCost()  << "   violations: " <<  s.getWeightViolations()<< endl;
		
		// cout << "LS ..." << endl;
		// LS_Program_Basic<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
		// lsprog.run(s, nm, 10000, 10, 30, true);			// args: (Solution &solution, NM &nm, int max_iter, int diversification = 10, int max_distance = 2500, bool verbose = false)
		
		LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
		// lsprog.set_Time_Callback(printBestSolutionCost, 1.0);
		// lsprog.set_NewBestSolution_Callback(newBestSolution);
		lsprog.setTimeOut(time_limit);
		lsprog.run(s, nm, numeric_limits<int>::max(), 5.0, 0.995, verbose);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)


		*bestSol = s;

		cout << endl << "Solution " << i << ":" << endl ;
		cout << bestSol->toString(false) << bestSol->getCost() <<  endl;

		s480->copyFromSol_scaleWaitingTimes(*bestSol);
		ASSERT(s480->getNumberViolations() == 0, s480->toString(true));
		cout << "Cost under scale 1 (" << recourse_strategy_str << ")" << endl << s480->toString(false) << s480->getCost() <<  endl;

		// VRP_ScenarioPool_Volatile<Scenario_SS_VRPTW_CR, Solution_SS_VRPTW_CR> scenarioPool_big(*I480, 1000000);
		// cout << "experimental expected cost = \t" << scenarioPool_big.computeExperimentalExpectedCost(*s480, strategy) << endl;

		Solution_SS_VRPTW_CR s_RCAPAPLUS(*I480, R_CAPA_PLUS);
		s_RCAPAPLUS.addRoutes(number_vehicles);
		s_RCAPAPLUS.generateInitialSolution(true);
		s_RCAPAPLUS.copyFromSol_scaleWaitingTimes(*s480);
		cout << "Cost under scale 1 (R_CAPA_PLUS) ==> " << endl << s_RCAPAPLUS.toString() << s_RCAPAPLUS.getCost() <<  endl;

		avg_cost += s480->getCost() / nb_runs;
		avg_cost_RCAPAPLUS += s_RCAPAPLUS.getCost() / nb_runs;
		cout << "=========================================================================" << endl;
	}
	cout << "###########################################################################" << endl;
	cout << "Avg cost / " << nb_runs << " runs: " << avg_cost << endl;
	cout << "RCAPAPLUS : Avg cost / " << nb_runs << " runs: " << avg_cost_RCAPAPLUS << endl;
	cout << "###########################################################################" << endl;
	// cout << s.toString(true) << endl;

	// cout << "theoretical expected cost (scale: " << scale << ") = ";// << outputMisc::blueBackExpr(true) << s.getCost() << outputMisc::resetColor();
	// cout << "\t\t\ttheoretical expected cost (scale: 1.0) = " << endl;
	

	// cout << bestSol.getCost();

	// cout << "\t\t\t\t\t\t\t\t" << cost480_bestSol << endl;

	// if (scale > 1) {
	// 	Solution_SS_VRPTW_CR bestSol_480(*I480, strategy);
	// 	bestSol_480.initializeFromSol_scaleWaitingTimes(bestSol, scale);
	
	// 	// // cout << s2.toString(false) << s2.toString(true) << endl;
	// 	// // cout << "\t\ttheoretical expected cost (scale: 1.0) = " << outputMisc::greenExpr(true) << s2.getCost() << outputMisc::resetColor()<< endl;
	// 	cout << "\t\t\t\t\t\t\t\t" << bestSol_480.getCost() << endl;
	// 	// // scenarioPool_big = new VRP_ScenarioPool_Volatile<Scenario_SS_VRPTW_CR, Solution_SS_VRPTW_CR>(I2, 1000000);
	// 	// // cout << "experimental expected cost = \t" << scenarioPool_big->computeExperimentalExpectedCost(s2, strategy) << endl;
		
	// 	// cout << endl;
	// } else cout << "\t\t\t\t\t\t\t\t" << bestSol.getCost();

	// Solution_SS_VRPTW_CR sQplus(*I480, R_CAPA_PLUS);
	// sQplus.initializeFromSol_scaleWaitingTimes(s, scale);
	// cout << endl << sQplus.getCost() << endl << sQplus.toString() << endl;
}



